<?php
/**
 * Created by PhpStorm.
 * User: Filip
 * Date: 5.3.2016
 * Time: 15:52
 */
/*
if (!isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER'] != 'cncmon' || $_SERVER['PHP_AUTH_PW'] != 'heslo') {
    header('WWW-Authenticate: Basic realm="cncmon"');
    header('HTTP/1.0 401 Unauthorized');
    echo 'Unauthorized';
    exit;
} else {
    echo "<p>Hello {$_SERVER['PHP_AUTH_USER']}.</p>";
    echo "<p>You entered {$_SERVER['PHP_AUTH_PW']} as your password.</p>";
}
*/
function __autoload($classname) {
    $filename = "./classes/". $classname .".php";
    include_once($filename);
}


//--------------------- MCV750 -------------------------------
$mcv750 = new Machine();
$mcv750->initMachine("mcv750");

$mcv750StoppedLight = new SignalLight();
$mcv750StoppedLight->initSignalLight("cam/cam6.jpg", 101, 43, 102, 44, true, 0, 60, 25, 100, 5);
$mcv750->setStoppedLight($mcv750StoppedLight);

//--------------------- MCV1000 -------------------------------

$mcv1000 = new Machine();
$mcv1000->initMachine("mcv1000");

$mcv1000StoppedLight = new SignalLight();
$mcv1000StoppedLight->initSignalLight("cam/cam11.jpg", 50, 22, 54, 24, true, 0, 60, 25, 100, 5);
$mcv1000->setStoppedLight($mcv1000StoppedLight);

$mcv1000WorkingLight = new SignalLight();
$mcv1000WorkingLight->initSignalLight("cam/cam11.jpg", 51, 25, 53, 28, false, 80, 165, 35, 100, 5);
$mcv1000->setWorkingLight($mcv1000WorkingLight);

//--------------------- Quaser -------------------------------

$quaser = new Machine();
$quaser->initMachine("quaser");

$quaserStoppedLight = new SignalLight();
$quaserStoppedLight->initSignalLight("cam/cam11.jpg", 210, 14, 212, 16, true, 0, 60, 20, 100, 5);
$quaser->setStoppedLight($quaserStoppedLight);

$quaserWorkingLight = new SignalLight();
$quaserWorkingLight->initSignalLight("cam/cam11.jpg", 210, 18, 213, 19, true, 80, 165, 25, 100, 5);
$quaser->setWorkingLight($quaserWorkingLight);

//--------------------- Lynx novy -------------------------------

$lynxNovy = new Machine();
$lynxNovy->initMachine("lynx_novy");

$lynxNovyStoppedLight = new SignalLight();
$lynxNovyStoppedLight->initSignalLight("cam/cam11.jpg", 280, 49, 282, 52, false, 0, 60, 25, 100, 5);
$lynxNovy->setStoppedLight($lynxNovyStoppedLight);

$lynxNovyWorkingLight = new SignalLight();
$lynxNovyWorkingLight->initSignalLight("cam/cam11.jpg", 281, 50, 283, 53, false, 80, 165, 25, 100, 5);
$lynxNovy->setWorkingLight($lynxNovyWorkingLight);

//--------------------- Lynx stary -------------------------------

$lynxVepredu = new Machine();
$lynxVepredu->initMachine("lynx_vepredu");

$lynxVepreduStoppedLight = new SignalLight();
$lynxVepreduStoppedLight->initSignalLight("cam/cam12.jpg", 206, 44, 209,48, false, 0, 60, 25, 100, 5);
$lynxVepredu->setStoppedLight($lynxVepreduStoppedLight);

$lynxVepreduWorkingLight = new SignalLight();
$lynxVepreduWorkingLight->initSignalLight("cam/cam12.jpg", 207, 31, 211, 36, false, 80, 165, 25, 100, 5);
$lynxVepredu->setWorkingLight($lynxVepreduWorkingLight);

//--------------------- Mori Seiki -------------------------------

$moriSeiki = new Machine();
$moriSeiki->initMachine("mori_seiki");

$moriSeikiStoppedLight = new SignalLight();
$moriSeikiStoppedLight->initSignalLight("cam/cam12.jpg", 86, 7, 89, 10, false, 0, 60, 25, 100, 5);
$moriSeiki->setStoppedLight($moriSeikiStoppedLight);

$moriSeikiWorkingLight = new SignalLight();
$moriSeikiWorkingLight->initSignalLight("cam/cam12.jpg", 86, 13, 89, 16, false, 80, 165, 25, 100, 5);
$moriSeiki->setWorkingLight($moriSeikiWorkingLight);


//--------------------- main -----------------------
$dataRepo = new DataRepository();
$dataRepo->connect('##########', '##########', '##########', '##########');

set_time_limit(60);
for ($i = 0; $i < 19; ++$i) {

    $mcv750->update($dataRepo);
    $mcv1000->update($dataRepo);
    $quaser->update($dataRepo);
    $lynxNovy->update($dataRepo);
    $lynxVepredu->update($dataRepo);
    $moriSeiki->update($dataRepo);

    sleep(3);
}

$dataRepo->close();


?>


