<?php
/**
 * Created by PhpStorm.
 * User: Filip
 * Date: 6.3.2016
 * Time: 19:51
 */

function __autoload($classname)
{
    if ($classname != "MachineState" || $classname != "LightsState") {
        $filename = "./classes/" . $classname . ".php";
        include_once($filename);
    }
}

$dataRepo = new DataRepository();
$dataRepo->connect('##########', '##########', '##########', '##########');

$machines = array("quaser", "mcv1000", "mcv750", "lynx_novy", "lynx_vepredu", "mori_seiki");
$colors = array("black", "green", "red", "blue");
?>

    <html>
    <head>
        <title>CNC Monitor</title>
        <meta http-equiv="refresh" content="3">
    </head>
    <body>
    <h1>CNC Monitor</h1>
    <table border="1px">
        <tr>
            <td>Machine</td>
            <td>Status</td>
            <td>Machine state update</td>
            <td>Last stop</td>
            <td>Cycle duration</td>
            <td>Countdown timer</td>
        </tr>
        <?php
        foreach ($machines as $machine) {
            $title = $dataRepo->getTitleByName($machine);
            $state = $dataRepo->getMachineStateByName($machine, true);
            $stateNum = $dataRepo->getMachineStateByName($machine, false);
            $updateTime = $dataRepo->getUpdateTimeByName($machine, true);
            $lastStop = $dataRepo->getLastStopByName($machine, true);
            $cycleduration = $dataRepo->getCycleDurationByName($machine);

            $lastStopDateTime = $dataRepo->getLastStopByName($machine, false);
            $nextStop = $lastStopDateTime->add(DateInterval::createFromDateString($cycleduration . ' seconds'));
            $remains = $nextStop->diff(new DateTime());
            $timer = ($state == "stopped" ? '00:00:00' : $remains->format('%H:%I:%S'));
            $timer = ($cycleduration ? $timer : '00:00:00');

            echo "
            <tr>
                <td>$title</td>
                <td style='color: $colors[$stateNum];'>$state</td>
                <td>$updateTime</td>
                <td>$lastStop</td>
                <td>$cycleduration</td>
                <td>$timer</td>
            </tr>";
        }

        ?>
    </table>
    <img src="cam/cam11.jpg"><img src="cam/cam6.jpg">
    </body>
    </html>

<?php

$dataRepo->close();
?>