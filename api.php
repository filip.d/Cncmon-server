<?php

function __autoload($classname)
{
    if ($classname != "MachineState" || $classname != "LightsState") {
        $filename = "./classes/" . $classname . ".php";
        include_once($filename);
    }
}


$dataRepo = new DataRepository();
$dataRepo->connect('####', '####', '####', '####');

//GET action
$action = $_GET["action"];

switch ($action) {
    case "registerDevice":
        $token = $_GET["token"];
        $gcm = new Gcm();

        if ($gcm->registerDevice($token)) {
            echo "1";
        }
        else
        {
            echo "0";
        }

        break;

    case "getMachineData":

        // GET machine names
        $machines = explode(",", $_GET["name"]);

        $response = array();
        $posts = array();

        foreach ($machines as $machine) {

         if (!$dataRepo->isValidMachineName($machine)) {
             // skip invalid names
            continue;
         }

         $title = $dataRepo->getTitleByName($machine);
         $state = $dataRepo->getMachineStateByName($machine, false);;
         $cycleDuration = $dataRepo->getCycleDurationByName($machine);
         $lastStopDateTime = $dataRepo->getLastStopByName($machine, false);
         $nextStop = ($cycleDuration ? date_format($lastStopDateTime->add(DateInterval::createFromDateString($cycleDuration . ' seconds')), 'Y-m-d H:i:s') : null);
         $cycleMonitoring = $dataRepo->isCycleMonitoringByName($machine);

         $posts[] = array('name' => $machine, 'title' => $title, 'state' => $state, 'next_stop' => $nextStop, 'cycle_monitoring' => $cycleMonitoring);

        }

        $response['machines'] = $posts;
        $json_response = json_encode($response, JSON_PRETTY_PRINT);

        echo $json_response;

        $dataRepo->close();
        die();

        break;
    case "sendTestPush":

        $gcm = new Gcm();
        $gcm->sendMessageToAll("quaser");

        break;
    default:
        echo "Invalid command";
}
?>

