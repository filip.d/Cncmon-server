<?php

/**
 * Created by PhpStorm.
 * User: Filip
 * Date: 6.3.2016
 * Time: 20:22
 */
class MachineState
{
    const NODATA = 0;
    const WORKING = 1;
    const STOPPED = 2;
    const ERROR = 3;
    const OFF = 4;
}