<?php

/**
 * Created by PhpStorm.
 * User: Filip
 * Date: 22.4.2016
 * Time: 14:54
 */
class Gcm
{
    private $dataRepo;

    const SERVER_API_KEY = "##########################";

    public function __construct()
    {
        $this->dataRepo = new DataRepository();
        $this->dataRepo->connect('#####', '#####', '#####', '#####');
    }

    public function registerDevice($token)
    {
        $id =  $this->dataRepo->addGcmToken($token);

        return ($id != null && $token ==  $this->dataRepo->getGcmTokenById($id));
    }

    public function sendMessageToAll($name)
    {

        $registrationIds =  $this->dataRepo->getGcmTokens();

        // prep the bundle
        $msg = array
        (
            'message' => $name,
            'title' => '',
            'subtitle' => '',
            'tickerText' => '',
            'vibrate' => 1,
            'sound' => 1,
            'largeIcon' => 'large_icon',
            'smallIcon' => 'small_icon'
        );
        $fields = array
        (
            'registration_ids' => $registrationIds,
            'data' => $msg
        );

        $headers = array
        (
            'Authorization: key=' . Gcm::SERVER_API_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        echo $result;

    }

}