<?php

/**
 * Created by PhpStorm.
 * User: Filip
 * Date: 6.3.2016
 * Time: 16:10
 */
class DataRepository
{
    /** @var  PDO $pdo*/
    private $pdo;


    public function __construct()
    {

    }

    public function connect($server, $username, $password, $db_name)
    {
        $dsn = 'mysql:dbname=' .  $db_name . ';host=' . $server . '';

        try
        {
            $this->pdo = new PDO($dsn, $username, $password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $e)
        {
            die('Connection failed: ' . $e->getMessage());
        }
    }

    public function close()
    {
        $this->pdo = null;
    }

    public function isValidMachineName($name) {
        $query = $this->pdo->prepare("SELECT * FROM machine WHERE name=?");
        try
        {
            $query->execute(array($name));
            $machineName = $query->fetchColumn();
        }
        catch (PDOException $e)
        {
            die('Chyba pri vycitani cycle_duration z DB. Message: ' . $e->getMessage());
        }

        return !empty($machineName);
    }

    public function getLightStatesLogByName($name)
    {
        if (!$this->isValidMachineName($name))
            return null;

        $query = $this->pdo->prepare("SELECT lights_state_log FROM machine WHERE name=?");
        try
        {
            $query->execute(array($name));
            $statesLog = $query->fetchColumn();
        }
        catch (PDOException $e)
        {
            die('Chyba pri vycitani lights_state_log z DB. Message: ' . $e->getMessage());
        }

        return $statesLog;
    }

    public function setLightStatesLogByName($name, $newStatesLog)
    {
        if (!$this->isValidMachineName($name))
            return null;

        $query = $this->pdo->prepare("UPDATE machine SET lights_state_log = ? WHERE name=?");
        try
        {
            $query->execute(array($newStatesLog,$name));
        }
        catch (PDOException $e)
        {
            die('Chyba pri ukladani machine_state do DB.. Message: ' . $e->getMessage());
        }
    }

    public function getMachineStateByName($name, $returnString)
    {
        if (!$this->isValidMachineName($name))
            return null;

        $query = $this->pdo->prepare("SELECT machine_state FROM machine WHERE name=?");
        try
        {
            $query->execute(array($name));
            $machineState = $query->fetchColumn();

            /** @var  MachineState $state */
            $state = intval($machineState);
            if ($returnString)
            {
                switch ($state)
                {
                    case MachineState::WORKING:
                        return "working";
                    case MachineState::STOPPED:
                        return "stopped";
                    case MachineState::NODATA:
                        return "no data";
                    case MachineState::ERROR:
                        return "error";
                    case MachineState::OFF:
                        return "off";
                }
            }
            else
            {
                return $state;
            }
        }
        catch (PDOException $e)
        {
            die('Chyba pri vycitani machine_State z DB. Message: ' . $e->getMessage());
        }
    }

    public function setMachineStateByName($name, $newState)
    {
        if (!$this->isValidMachineName($name))
            return null;

        $query = $this->pdo->prepare("UPDATE machine SET machine_state = ? WHERE name=?");
        try
        {
            $query->execute(array($newState,$name));
        }
        catch (PDOException $e)
        {
            die('Chyba pri ukladani machine_state do DB.. Message: ' . $e->getMessage());
        }
    }

    public function getTitleByName($name)
    {
        if (!$this->isValidMachineName($name))
            return null;

        $query = $this->pdo->prepare("SELECT title FROM machine WHERE name=?");
        try
        {
            $query->execute(array($name));
            $title = $query->fetchColumn();
        }
        catch (PDOException $e)
        {
            die('Chyba pri vycitani lights_state_log z DB. Message: ' . $e->getMessage());
        }
        return $title;
    }

    public function setUpdateTimeByName($name, $datetime)
    {
        if (!$this->isValidMachineName($name))
            return null;

        $query = $this->pdo->prepare("UPDATE machine SET machine_state_update = ? WHERE name=?");
        try
        {
            $query->execute(array($datetime,$name));
        }
        catch (PDOException $e)
        {
            die('Chyba pri ukladani update_time do DB.. Message: ' . $e->getMessage());
        }
    }

    public function getUpdateTimeByName($name, $returnString, $outFormat = 'Y-m-d H:i:s')
    {
        if (!$this->isValidMachineName($name))
            return null;

        $query = $this->pdo->prepare("SELECT machine_state_update FROM machine WHERE name=?");
        try {
            $query->execute(array($name));
            $datetime = $query->fetchColumn();
        } catch (PDOException $e)
        {
            die('Chyba pri vycitani update_time z DB. Message: ' . $e->getMessage());
        }

        if (!$datetime) {
            $dateRet =  new DateTime();
        }
        else
        {
            $dateRet = DateTime::createFromFormat('Y-m-d H:i:s', $datetime);
        }

        if ($returnString)
            return date_format($dateRet, $outFormat);
        else
            return $dateRet;
    }

    public function setLastStopByName($name, $datetime)
    {
        if (!$this->isValidMachineName($name))
            return null;

        $query = $this->pdo->prepare("UPDATE machine SET last_stop = ? WHERE name=?");
        try
        {
            $query->execute(array($datetime,$name));
        }
        catch (PDOException $e)
        {
            die('Chyba pri ukladani astStop do DB.. Message: ' . $e->getMessage());
        }
    }

    public function getLastStopByName($name, $returnString, $outFormat = 'Y-m-d H:i:s')
    {
        if (!$this->isValidMachineName($name))
            return null;

        $query = $this->pdo->prepare("SELECT last_stop FROM machine WHERE name=?");
        try {
            $query->execute(array($name));
            $datetime = $query->fetchColumn();
        } catch (PDOException $e)
        {
            die('Chyba pri vycitani last_stop z DB. Message: ' . $e->getMessage());
        }

        if (!$datetime) {
            return null;
        }
        else
        {
            $dateRet = DateTime::createFromFormat('Y-m-d H:i:s', $datetime);
        }

        if ($returnString)
            return date_format($dateRet, $outFormat);
        else
            return $dateRet;
    }

    public function setCycleDurationByName($name, $cycleduration)
    {
        if (!$this->isValidMachineName($name))
            return null;

        $query = $this->pdo->prepare("UPDATE machine SET cycle_duration = ? WHERE name=?");
        try
        {
            $query->execute(array($cycleduration, $name));
        }
        catch (PDOException $e)
        {
            die('Chyba pri ukladani cycle_duration do DB.. Message: ' . $e->getMessage());
        }
    }

    public function getCycleDurationByName($name)
    {
        if (!$this->isValidMachineName($name))
            return null;

        $query = $this->pdo->prepare("SELECT cycle_duration FROM machine WHERE name=?");
        try
        {
            $query->execute(array($name));
            $cycleduration = $query->fetchColumn();
        }
        catch (PDOException $e)
        {
            die('Chyba pri vycitani cycle_duration z DB. Message: ' . $e->getMessage());
        }

        return $cycleduration;
    }

    public function setCycleMonitoringByName($name, $cycleMonitoring)
    {
        if (!$this->isValidMachineName($name))
            return null;

        $query = $this->pdo->prepare("UPDATE machine SET cycle_monitoring = ? WHERE name=?");
        try
        {
            $query->execute(array($cycleMonitoring, $name));
        }
        catch (PDOException $e)
        {
            die('Chyba pri ukladani cycle_monitoring do DB.. Message: ' . $e->getMessage());
        }
    }

    public function isCycleMonitoringByName($name)
    {
        if (!$this->isValidMachineName($name))
            return null;

        $query = $this->pdo->prepare("SELECT cycle_monitoring FROM machine WHERE name=?");
        try
        {
            $query->execute(array($name));
            $cycleMonitoring = $query->fetchColumn();
        }
        catch (PDOException $e)
        {
            die('Chyba pri vycitani cycle_monitoring z DB. Message: ' . $e->getMessage());
        }

        return intval($cycleMonitoring);
    }

    public function addGcmToken($token)
    {
        // zkontrolovat jestli token jiz v databazi neni
        $exists = $this->pdo->prepare('SELECT * FROM clients WHERE token=?');
        $exists->execute(array($token));;
        $row = $exists->fetch(PDO::FETCH_ASSOC);

        if($row)
        {
           echo('Token je již v databázi uložen.');
           return null;
        }

        // pridat token do databaze
        $query = $this->pdo->prepare("INSERT INTO clients (token) VALUES (?)");
        try
        {
            $query->execute(array($token));
            $id = $this->pdo->lastInsertId();
        }
        catch (PDOException $e)
        {
            echo('Chyba pri pridavani tokenu do DB. Message: ' . $e->getMessage());
            return null;
        }

        return $id;
    }

    public function getGcmTokenById($id)
    {
        $query = $this->pdo->prepare("SELECT token FROM clients WHERE id=?");
        try
        {
            $query->execute(array($id));
            $token = $query->fetchColumn();
        }
        catch (PDOException $e)
        {
            echo('Chyba pri vycitani tokenu z DB. Message: ' . $e->getMessage());
            return null;
        }

        return $token;
    }

    public function getGcmTokens()
    {
    $query = $this->pdo->prepare("SELECT token FROM clients");
    try
    {
        $query->execute();
        $tokens = $query->fetchAll(PDO::FETCH_COLUMN, 0);
    }
    catch (PDOException $e)
    {
        die('Chyba pri vycitani tokenu z DB. Message: ' . $e->getMessage());
    }

    return $tokens;
    }

}

