<?php

/**
 * Created by PhpStorm.
 * User: Filip
 * Date: 5.3.2016
 * Time: 14:20
 */
class Machine
{
    private $name;
    private $machineState;
    private $lightsState;

    /** @var SignalLight */
    private $workingLight;

    /** @var  SignalLight */
    private $stoppedLight;

    private $isWorkingMonitoring;

    public function __construct()
    {
    }

    public function initMachine($name)
    {
        $this->name = $name;
        $this->isWorkingMonitoring = $isWorkingMonitoring = false;
        $this->machineState = MachineState::OFF;
    }

    /**
     * @param SignalLight $signalLight
     */
    public function setWorkingLight(SignalLight $signalLight)
    {
        $this->workingLight = $signalLight;
        $this->isWorkingMonitoring = true;
    }

    /**
     * @param SignalLight $signalLight
     */
    public function setStoppedLight(SignalLight $signalLight)
    {
        $this->stoppedLight = $signalLight;
    }

    public function updateLights()
    {
        //echo "<br><b>$this->name</b><br> ";

        /** @var SignalLight[] $lights */
        $lights = array($this->stoppedLight);
        if ($this->isWorkingMonitoring)
            $lights[] = $this->workingLight;

        foreach ($lights as &$light) {
            list($r, $g, $b) = $light->getAverageRGBColorInRect();
            list($h, $s, $l) = $light->rgbToHsl($r, $g, $b);
            list($L, $A, $B) = $light->rgbToLab($r, $g, $b);

            // pokud je (L, A, B) = (0, 0, 0), nebyl spravne nacteny obraz
            if ($L && $A && $B) {
                //pokud se jedna o prvni spusteni scriptu (jeste neexistuje predchozi hodnota)
                if ($light->getLastLab()) {
                    list($L2, $A2, $B2) = $light->getLastLab();
                    $colorDifference = sqrt(($L2 - $L) ** 2 + ($A2 - $A) ** 2 + ($B2 - $B) ** 2);
                    //echo "Color difference: $colorDifference <br>";

                    //tato hodnota (20) by asi mela byt parametr
                    if ($colorDifference > 20) {
                        $light->setLightStateChanged(true);
                    } else {
                        $light->setLightStateChanged(false);
                    }
                    $light->setLastLab($L, $A, $B);
                } else {
                    $light->setLastLab($L, $A, $B);
                }
            }

            $hueInRange = ($h >= $light->getHueL() && $h <= $light->getHueH()) || ($light->getHueL() <= 20 && $h >= 285 && $h <= 360); //cervena ma navic hue i na druhe strane spektra
            $lightInRange = $l >= $light->getLightL() && $l <= $light->getLightH();
            $satInRange = $s >= $light->getSatL();

            if ($hueInRange && $lightInRange && $satInRange) {
                $light->setLightState(true);
            } else {
                $light->setLightState(false);
            }
        }
        unset($light);

        //echo "Stopped H: $h, S: $s, L: $l
    }

    public function updateLightsState()
    {
        $this->updateLights();
        $this->lightsState = LightsState::NONE;

        if ($this->stoppedLight->isBlinking()) {
            if ($this->stoppedLight->getLightStateChanged() && ($this->stoppedLight->getLightState() || $this->stoppedLight->getLastLightState())) {
                $this->lightsState = LightsState::STOPPED;
            }
        } else {
            if ($this->stoppedLight->getLightState()) {
                $this->lightsState = LightsState::STOPPED;
            }
        }
        if ($this->isWorkingMonitoring) {
            if ($this->workingLight->isBlinking()) {
                if ($this->workingLight->getLightStateChanged() && ($this->workingLight->getLightState() || $this->workingLight->getLastLightState())) {
                    if ($this->lightsState == LightsState::STOPPED) {
                        $this->lightsState = LightsState::BOTH;;
                    } else {
                        $this->lightsState = LightsState::WORKING;
                    }
                }
            } else {
                if ($this->workingLight->getLightState()) {
                    if ($this->lightsState == LightsState::STOPPED) {
                        $this->lightsState = LightsState::BOTH;
                    } else {
                        $this->lightsState = LightsState::WORKING;
                    }
                }
            }

            $this->workingLight->setLastLightState($this->workingLight->getLightState());
        }
        $this->stoppedLight->setLastLightState($this->stoppedLight->getLightState());
    }

    public function getLightState()
    {
        return $this->lightsState;
    }

    /** @var DataRepository $dataRepo */
    public function update($dataRepo)
    {
        $previousMachineState = $dataRepo->getMachineStateByName($this->name, false);
        $this->updateLightsState();

        $newStatesLog = substr_replace($dataRepo->getLightStatesLogByName($this->name), $this->lightsState, 0, 0);

        // maximalni delka zaznamu = 60 stavu
        if (strlen($newStatesLog) > 60) {
            $newStatesLog = substr($newStatesLog, 0, -1);
        }
        //pridej lightState obohaceny o novy stav do databaze pro $name
        $dataRepo->setLightStatesLogByName($this->name, $newStatesLog);

        $noneCount = 0;
        $stoppedCount = 0;
        $workingCount = 0;
        $bothCount = 0;

        //vybrat poslednich 12 zaznamu
        $statesLogLength = 14;
        $statesArray = str_split(substr($newStatesLog, 0, $statesLogLength));

        foreach ($statesArray as $state) {
            switch ($state) {
                case LightsState::NONE:
                    $noneCount++;
                    break;
                case LightsState::STOPPED:
                    $stoppedCount++;
                    break;
                case LightsState::WORKING:
                    $workingCount++;
                    break;
                case LightsState::BOTH:
                    $bothCount++;
                    break;
            }
        }
        $minFlags = 3;
        $maxError = 2;   // both lights
        $previousStateChange = $dataRepo->getUpdateTimeByName($this->name, false);
        $now = new DateTime();
        $stateChangeDiff = $previousStateChange->diff($now);

        // 0000110100101101
        if ($workingCount >= $minFlags && $bothCount <= $maxError && !$stoppedCount) {
            $this->machineState = MachineState::WORKING;
        } // 2022200020200202
        elseif ($stoppedCount >= $minFlags && $bothCount <= $maxError && !$workingCount) {
            $this->machineState = MachineState::STOPPED;

        } // nepracuje, nestoji a nebylo zaznamenana zadna zmena po dobu tri hodin
        elseif ($stateChangeDiff->h >= 3) {
            $this->machineState = MachineState::OFF;
        } // 0000000000000000
        elseif ($noneCount == $statesLogLength) {
            $this->machineState = MachineState::NODATA;
        } else {
            $this->machineState = $previousMachineState;
        }

        // pokud doslo ke zmene stavu
        if ($this->machineState != $previousMachineState) {
            $dataRepo->setMachineStateByName($this->name, $this->machineState);
            $dataRepo->setUpdateTimeByName($this->name, date('Y-m-d H:i:s'));

            if ($this->machineState == MachineState::STOPPED) {
                $lastStop = $dataRepo->getLastStopByName($this->name, false);
                $now = new DateTime();
                $cycleDuration = $now->getTimestamp() - $lastStop->getTimestamp();

                // hystereze min 5 minut (tak kratky cyklus nedovoli odejit od frezky), max 10 hod
                if ($cycleDuration > 300 && $cycleDuration < 36000) {
                    $dataRepo->setCycleDurationByName($this->name, $cycleDuration);
                }

                // odesle pushku
                $gcm = new Gcm();
                $gcm->sendMessageToAll($this->name);

            } // vyresetovat cycle duration pokud masina prejde do stavu OFF
            elseif ($this->machineState == MachineState::OFF) {
                $dataRepo->setCycleDurationByName($this->name, 0);
            }

            // zapise cas last_stop po tom, co prestane byt masina ve stavu STOPPED
            if ($previousMachineState == MachineState::STOPPED) {
                $dataRepo->setLastStopByName($this->name, date('Y-m-d H:i:s'));
            }


        }
    }

    public function getStatus()
    {
        // zde bude vycten stav stroje z databaze
        return $this->machineState;     //$this->machineState;
    }

    public function drawAverageRGB()
    {
        switch ($this->getStatus()) {
            case MachineState::WORKING:
                list($r, $g, $b) = $this->workingLight->getAverageRGBColorInRect();
                $this->workingLight->drawCroppedImage();
                echo "<div style='width: 60px; height: 60px; background-color: rgb($r, $g, $b);'>&nbsp</div>";
                break;
            case MachineState::STOPPED:
                list($r, $g, $b) = $this->stoppedLight->getAverageRGBColorInRect();
                $this->stoppedLight->drawCroppedImage();
                echo "<div style='width: 60px; height: 60px; background-color: rgb($r, $g, $b);'>&nbsp</div>";
                break;
            case MachineState::OFF:
                list($r, $g, $b) = $this->stoppedLight->getAverageRGBColorInRect();
                $this->stoppedLight->drawCroppedImage();
                echo "<div style='width: 60px; height: 60px; background-color: rgb($r, $g, $b);'>&nbsp</div>";
                break;
            default:
                break;
        }
    }

    public function echoStatus()
    {
        switch ($this->getStatus()) {
            case MachineState::WORKING:
                echo "working";
                break;
            case MachineState::STOPPED:
                echo "stopped";
                break;
            case MachineState::NODATA:
                echo "no data";
                break;
            case MachineState::ERROR:
                echo "error";
                break;
            case MachineState::OFF:
                echo "off";
                break;
        }
    }
}