<?php

/**
 * Created by PhpStorm.
 * User: Filip
 * Date: 5.3.2016
 * Time: 13:48
 */
class SignalLight
{
    private $originX;
    private $originY;
    private $destX;
    private $destY;

    private $image;
    private $croppedImage;

    private $blinking;

    private $hueL;
    private $hueH;
    private $lightL;
    private $lightH;
    private $satL;

    private $lightState;
    private $lastLightState;
    private $lightStateChanged;

    private $lastLab = false;

    function __construct()
    {

    }

    public function initSignalLight($imgPath, $originX, $originY, $destX, $destY, $blinking, $hueL, $hueH, $lightL, $lightH, $satL)
    {
        $this->originX = $originX;
        $this->originY = $originY;
        $this->destX = $destX;
        $this->destY = $destY;
        $this->blinking = $blinking;
        $this->imgPath = $imgPath;
        $this->hueL = $hueL;
        $this->hueH = $hueH;
        $this->lightL = $lightL;
        $this->lightH = $lightH;
        $this->satL = $satL;
    }

    public function loadImg()
    {
        // zkusit nejak osetrit to nacitani neuplnych jpeg souboru
        try {
            $this->image = @imagecreatefromjpeg($this->imgPath);
        } catch (Exception $e) {

        }

    }

    public function getAverageRGBColorInRect()
    {
        //nacist obraz z kamery
        $this->loadImg();

        //oreze obraz pouze na oblast, kde je majak
        $croppedWidth = $this->destX - $this->originX + 1;
        $croppedHeight = $this->destY - $this->originY + 1;
        $croppedImage = imagecreatetruecolor($croppedWidth, $croppedHeight);
        @imagecopyresampled($croppedImage, $this->image, 0, 0, $this->originX, $this->originY, $croppedWidth, $croppedHeight, $croppedWidth, $croppedHeight);
        $this->croppedImage = $croppedImage;

        //zmensi obrazek na 2x2 pixelu, tim dojde k zprumerovani barev (nevim proc nefunguje 1x1 pixel)
        $onepixel = imagescale($croppedImage, 2, 2, IMG_BICUBIC);

        //zjistit barvu pixelu a rozmrda ji na jednotlive kanaly
        $rgb = imagecolorat($onepixel, 0, 0);
        $r = ($rgb >> 16) & 0xFF;
        $g = ($rgb >> 8) & 0xFF;
        $b = $rgb & 0xFF;

        return array($r, $g, $b);
    }

    public function rgbToHsl($r, $g, $b)
    {
        //convesrion formula http://www.rapidtables.com/convert/color/rgb-to-hsl.htm
        $r /= 255;
        $g /= 255;
        $b /= 255;

        $max = max($r, $g, $b);
        $min = min($r, $g, $b);

        $h = 0;
        $s = 0;
        $l = ($max + $min) / 2;
        $d = $max - $min;

        if ($d == 0) {
            $h = $s = 0; // achromatic
        } else {
            $s = $d / (1 - abs(2 * $l - 1));

            switch ($max) {
                case $r:
                    $h = 60 * fmod((($g - $b) / $d), 6);
                    if ($b > $g) {
                        $h += 360;
                    }
                    break;
                case $g:
                    $h = 60 * (($b - $r) / $d + 2);
                    break;
                case $b:
                    $h = 60 * (($r - $g) / $d + 4);
                    break;
            }
        }

        return array(round($h, 2), round($s, 2) * 100, round($l, 2) * 100);
    }

    public function rgbToLab($r, $g, $b)
    {
        //CIE L*a*b je barevny model vhodny k porovnavani barev

        //RGB to XYZ
        $var_R = ($r / 255);        //R from 0 to 255
        $var_G = ($g / 255);       //G from 0 to 255
        $var_B = ($b / 255);       //B from 0 to 255

        if ($var_R > 0.04045)
            $var_R = (($var_R + 0.055) / 1.055) ** 2.4;
        else
            $var_R = $var_R / 12.92;

        if ($var_G > 0.04045)
            $var_G = (($var_G + 0.055) / 1.055) ** 2.4;
        else
            $var_G = $var_G / 12.92;

        if ($var_B > 0.04045)
            $var_B = (($var_B + 0.055) / 1.055) ** 2.4;
        else
            $var_B = $var_B / 12.92;

        $var_R = $var_R * 100;
        $var_G = $var_G * 100;
        $var_B = $var_B * 100;

        //Observer. = 2°, Illuminant = D65
        $X = $var_R * 0.4124 + $var_G * 0.3576 + $var_B * 0.1805;
        $Y = $var_R * 0.2126 + $var_G * 0.7152 + $var_B * 0.0722;
        $Z = $var_R * 0.0193 + $var_G * 0.1192 + $var_B * 0.9505;

        //XYZ to LAB
        $var_X = $X / 95.047; // Observer= 2°, Illuminant= D65
        $var_Y = $Y / 100.000;
        $var_Z = $Z / 108.883;

        if ($var_X > 0.008856)
            $var_X = $var_X ** (1 / 3);
        else
            $var_X = (7.787 * $var_X) + (16 / 116);
        if ($var_Y > 0.008856)
            $var_Y = $var_Y ** (1 / 3);
        else
            $var_Y = (7.787 * $var_Y) + (16 / 116);
        if ($var_Z > 0.008856)
            $var_Z = $var_Z ** (1 / 3);
        else
            $var_Z = (7.787 * $var_Z) + (16 / 116);

        $L = (116 * $var_Y) - 16;
        $a = 500 * ($var_X - $var_Y);
        $b = 200 * ($var_Y - $var_Z);

        return array($L, $a, $b);
    }

    public function isBlinking()
    {
        return $this->blinking;
    }

    public function getHueL()
    {
        return $this->hueL;
    }

    public function getHueH()
    {
        return $this->hueH;
    }

    public function getLightL()
    {
        return $this->lightL;
    }

    public function getLightH()
    {
        return $this->lightH;
    }

    public function getSatL()
    {
        return $this->satL;
    }

    public function setLightState($state)
    {
        $this->lightState = $state;
    }

    public function getLightState()
    {
        return $this->lightState;
    }

    public function setLastLightState($state)
    {
        $this->lastLightState = $state;
    }

    public function getLastLightState()
    {
        return $this->lastLightState;
    }

    public function setLightStateChanged($changed)
    {
        $this->lightStateChanged = $changed;
    }

    public function getLightStateChanged()
    {
        return $this->lightStateChanged;
    }

    public function setLastLab($l, $a, $b)
    {
        $this->lastLab = array($l, $a, $b);
    }

    public function getLastLab()
    {
        return $this->lastLab;
    }


    //docasna funkce, slouzi k ladeni a optimalizaci orezavane oblasti
    public function drawCroppedImage()
    {
        $name = strval($this->originY) . 'thumb.jpg';
        imagejpeg($this->croppedImage, $name);
        echo "<img src='$name'>";
    }
}