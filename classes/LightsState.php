<?php

/**
 * Created by PhpStorm.
 * User: Filip
 * Date: 6.3.2016
 * Time: 20:22
 */
class LightsState
{
    const NONE = 0;
    const WORKING = 1;
    const STOPPED = 2;
    const BOTH = 3;
}